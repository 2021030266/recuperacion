package Controlador;
/**
 *
 * @author Manuel
 */
import Modelo.Recibo;
import vista.dlgvista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    private Recibo recibo;
    private dlgvista vista;

    public Controlador(Recibo recibo, dlgvista vista) {
        this.recibo = recibo;
        this.vista = vista;

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);

        vista.boxTipoServicio.addActionListener(this);
    }

    private void iniciarVista() {
        vista.setTitle(":: Control de Pagos ::");
        vista.setSize(425, 520);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            vista.txtNumRecibo.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.boxTipoServicio.setEnabled(true);
            vista.txtKilowattsConsumidos.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            limpiarCampos();
        } 
        else if (e.getSource() == vista.btnGuardar) {
            if (camposLlenos()) {
                try {
                    int numRecibo = Integer.parseInt(vista.txtNumRecibo.getText());
                    String fecha = vista.txtFecha.getText();
                    String nombre = vista.txtNombre.getText();
                    String domicilio = vista.txtDomicilio.getText();
                    int tipoServicio = vista.boxTipoServicio.getSelectedIndex() + 1;
                    float costoKilowatts = Float.parseFloat(vista.txtCostosKilowatts.getText());
                    int kilowattsConsumidos = Integer.parseInt(vista.txtKilowattsConsumidos.getText());

                    recibo.setNumRecibo(numRecibo);
                    recibo.setFecha(fecha);
                    recibo.setNombre(nombre);
                    recibo.setDomicilio(domicilio);
                    recibo.setTipoServicio(tipoServicio);
                    recibo.setCostokilowatts(costoKilowatts);
                    recibo.setKilowatts(kilowattsConsumidos);

                    JOptionPane.showMessageDialog(vista, "Registro Exitoso.");
                     vista.btnMostrar.setEnabled(true);
                    limpiarCampos();
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "Ocurrió un error al guardar el registro.\nPor favor, verifique los valores ingresados.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(vista, "Por favor, complete todos los campos.", "Campos incompletos", JOptionPane.WARNING_MESSAGE);
            }
        }
        else if (e.getSource() == vista.btnMostrar) {
            vista.txtNumRecibo.setText(String.valueOf(recibo.getNumRecibo()));
            vista.txtFecha.setText(recibo.getFecha());
            vista.txtNombre.setText(recibo.getNombre());
            vista.txtDomicilio.setText(recibo.getDomicilio());
            vista.txtCostosKilowatts.setText(String.valueOf(recibo.getCostokilowatts()));
            vista.txtKilowattsConsumidos.setText(String.valueOf(recibo.getKilowatts()));
            vista.boxTipoServicio.setSelectedIndex(recibo.getTipoServicio() - 1);

            double subtotal = recibo.calcularSubtotal();
            double impuestos = recibo.calcularImpuestos();
            double totalPagar = recibo.calcularTotalPagar();

            vista.txtSubtotal.setText(String.valueOf(subtotal));
            vista.txtImpuestos.setText(String.valueOf(impuestos));
            vista.txtTotalPagar.setText(String.valueOf(totalPagar));
            deshabilitarCampos();     
        }  
        else if (e.getSource() == vista.boxTipoServicio) {
            int tipoServicio = vista.boxTipoServicio.getSelectedIndex();
            recibo.actualizarPrecioKilowatts(tipoServicio);
            float costoKilowatts = recibo.getCostokilowatts();
            vista.txtCostosKilowatts.setText(String.valueOf(costoKilowatts));
            vista.txtCostosKilowatts.setEnabled(tipoServicio != 2);
        }
        else if (e.getSource() == vista.btnLimpiar) {
            limpiarCampos();
        }
        else if (e.getSource() == vista.btnCancelar) {
            deshabilitarCampos();
        }   
        else if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Desea cerrar la aplicación?", "Cerrar", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        } 
    }

    private void limpiarCampos() {
        vista.txtNumRecibo.setText("");
        vista.txtFecha.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.boxTipoServicio.setSelectedIndex(0);
        vista.txtCostosKilowatts.setText("");
        vista.txtKilowattsConsumidos.setText("");
        vista.txtSubtotal.setText("");
        vista.txtImpuestos.setText("");
        vista.txtTotalPagar.setText("");
    }

    private void deshabilitarCampos() {
        vista.txtNumRecibo.setEnabled(false);
        vista.txtFecha.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtDomicilio.setEnabled(false);
        vista.boxTipoServicio.setEnabled(false);
        vista.txtCostosKilowatts.setEnabled(false);
        vista.txtKilowattsConsumidos.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
    }
    
    private boolean camposLlenos() {
        String numRecibo = vista.txtNumRecibo.getText();
        String fecha = vista.txtFecha.getText();
        String nombre = vista.txtNombre.getText();
        String domicilio = vista.txtDomicilio.getText();
        String costoKilowatts = vista.txtCostosKilowatts.getText();
        String kilowattsConsumidos = vista.txtKilowattsConsumidos.getText();

        return !numRecibo.isEmpty() && !fecha.isEmpty() && !nombre.isEmpty() && !domicilio.isEmpty()
                && !costoKilowatts.isEmpty() && !kilowattsConsumidos.isEmpty();
    }
    
    public static void main(String[] args) {
        Recibo recibo = new Recibo();
        dlgvista vista = new dlgvista();
        Controlador controlador = new Controlador(recibo, vista);
        controlador.iniciarVista();
    }
}
