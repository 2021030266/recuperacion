package Modelo;
/**
 *
 * @author Manuel
 */
public class Recibo {
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private float kilowatts;
    private float costokilowatts;
    
    public Recibo(int numRecibo, String nombre, String domicilio, int tipoServicio, float costoKilo, float kiloConsu , String fecha) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipoServicio = tipoServicio;
        this.costokilowatts = costoKilo;
        this.kilowatts = kiloConsu;
        this.fecha =  fecha;
    }

    public Recibo() {
        this.numRecibo = 0;
        this.domicilio = null;
        this.nombre = null; 
        this.tipoServicio= 0;
        this.costokilowatts = 0.0f;
        this.kilowatts = 0.0f;
        this.fecha = null;
    }
    
    public Recibo(Recibo rec){
        this.numRecibo = rec.numRecibo;
        this.domicilio = rec.domicilio;
        this.nombre =  rec.nombre;
        this.tipoServicio= rec.tipoServicio;
        this.costokilowatts = rec.costokilowatts;
        this.kilowatts = rec.kilowatts;
        this.fecha = rec.fecha;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostokilowatts() {
        return costokilowatts;
    }

    public void setCostokilowatts(float costokilowatts) {
        this.costokilowatts = costokilowatts;
    }

    public float getKilowatts() {
        return kilowatts;
    }

    public void setKilowatts(float kilowatts) {
        this.kilowatts = kilowatts;
    }

    public float calcularSubtotal() {
        return costokilowatts * kilowatts;
    }

    public float calcularImpuestos() {
        return this.calcularSubtotal() * 0.16f;
    }

    public float calcularTotalPagar() {
        return this.calcularSubtotal() + this.calcularImpuestos();
    }

    public void actualizarPrecioKilowatts(int tipoServicio) {
           switch (tipoServicio) {
               case 0: // Servicio Residencial
                   costokilowatts = 2f;
                   break;
               case 1: // Servicio Comercial
                   costokilowatts = 3f;
                   break;
               case 2: // Servicio Industrial
                   costokilowatts = 5f; 
                   break;
               default:
                   costokilowatts = 0;
                   break;
           }
       }

}
